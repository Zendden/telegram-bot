<?php 

namespace App\Services;

use App\Entity\User;
use App\Services\UserService;
use App\Services\ActionsService;

class UpdateEventService 
{
    /**
     * @var ActionsService
     */
    private $actionsService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var QueueMessageService
     */
    private $queueMessageService;

    /**
     * @var TelegramBotService
     */
    private $telegramBotService;

    /**
     * @var DataValidatorService
     */
    private $dataValidatorService;

    /**
     * @param ActionsService $actionsService
     * @param UserService $userService
     * @param QueueMessageService $queueMessageService
     * @param TelegramBotService $telegramBotService
     * @param DataValidatorService $dataValidatorService
     */
    public function __construct(
        ActionsService $actionsService,
        UserService $userService,
        QueueMessageService $queueMessageService,
        TelegramBotService $telegramBotService,
        DataValidatorService $dataValidatorService
    ){
        $this->actionsService = $actionsService;
        $this->userService = $userService;
        $this->queueMessageService = $queueMessageService;
        $this->telegramBotService = $telegramBotService;
        $this->dataValidatorService = $dataValidatorService;
    }

    /**
     * @param array $updateEvents
     */
    public function processUpdates(array $updateEvents): void 
    {
        $newUsers = [];
        foreach ($updateEvents as $updateEvent) {
            /** @var User|bool */
            $user = $this->userService->getUser($updateEvent);

            if (!$user) {
                /** @var User */
                $user = $this->userService->createUser($updateEvent);

                array_push($newUsers, $user);
            } else {
                $this->processUpdateAction($updateEvent, $user);
            }

            $this->queueMessageService->addQueueMessage($updateEvent ,$user);
        }

        $this->actionsService->welcomeNewUsers($newUsers);
    }

    /**
     * @param array $updateEvent
     * @param User $user
     */
    public function processUpdateAction(array $updateEvent, User $user): void 
    {
        $message = $this->dataValidatorService->get(
            $updateEvent, 
            ['message', 'text'], 
            DataValidatorService::STRICT_MODE
        );

        $this->telegramBotService->sendMessage([
            'chat_id' => $user->getChat()->getChatId(),
            'text' => $this->actionsService->translate($message)
        ]);
    }
}