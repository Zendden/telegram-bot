<?php 

namespace App\Services;

use App\Entity\Chat;
use App\Services\DataValidatorService;

class ChatService 
{
    /**
     * @var DataValidatorService
     */
    private $dataValidatorService;

    /**
     * @param DataValidatorService $dataValidatorService
     */
    public function __construct(DataValidatorService $dataValidatorService){
        $this->dataValidatorService = $dataValidatorService;
    }

    /**
     * @param array $chatData
     * 
     * @return Chat
     */
    public function buildChat(array $chatData): Chat 
    {
        return (new Chat())
            ->setChatId(
                $this->dataValidatorService->get(
                    $chatData, 
                    ['id'],
                    DataValidatorService::STRICT_MODE
                )
            )
            ->setCreatedAt(new \DateTime());
    }
}