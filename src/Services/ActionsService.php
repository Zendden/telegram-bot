<?php 

namespace App\Services;

use App\Entity\User;
use Dejurin\GoogleTranslateForFree;
use App\Services\TelegramBotService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ActionsService 
{
    public const RU_LANG = 'ru';
    public const EN_LANG = 'en';

    /**
     * @var TelegramBotService
     */
    private $telegramBotService;

    /**
     * @var string
     */
    private $botName;

    /**
     * @param TelegramBotService $telegramBotService
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        TelegramBotService $telegramBotService,
        ParameterBagInterface $parameterBag
    ){
        $this->botName = $parameterBag->get('bot-name');

        $this->telegramBotService = $telegramBotService;
    }

    /**
     * @param User[] $chatData
     */
    public function welcomeNewUsers(array $users): void 
    {
        foreach ($users as $user) {
            $this->telegramBotService->sendMessage(
                [
                    'chat_id' => $user->getChat()->getChatId(),
                    'text' => sprintf(
                        '%s, welcome to %s bot!',
                        $user->getUsername(),
                        $this->botName
                    )
                ]
            );

            $this->telegramBotService->sendMessage(
                [
                    'chat_id' => $user->getChat()->getChatId(),
                    'text' => $this->buildActionsList()
                ]
            );
        }
    }

    /**
     * Text for Tranlating.
     * @param string $text
     * 
     * Attempts for Translating.
     * @param int $attempts
     * 
     * @return string
     */
    public function translate(string $text, int $attempts = 1): string 
    {
        $translator = new GoogleTranslateForFree();

        $translatedText = $translator->translate(
            self::RU_LANG, 
            self::EN_LANG,
            $text,
            $attempts
        );

        if ($translatedText === $text) {
            $translatedText = $translator->translate(
                self::EN_LANG, 
                self::RU_LANG, 
                $text,
                $attempts
            );
        }

        return $translatedText;
    }

    /**
     * @return string
     */
    public function buildActionsList(): string 
    {
        $actionsList = [
            'Translate from RU to ENG/ENG to RU text' => '{Just enter the text in target RU/ENG language}'
        ];

        $actionBuiltList = sprintf('Actions: %s', PHP_EOL . PHP_EOL);
        foreach ($actionsList as $actionDescription => $actionCommand) {
            $actionBuiltList .= sprintf(
                '%s -> %s%s', 
                $actionDescription, 
                $actionCommand, 
                PHP_EOL
            );
        }

        return $actionBuiltList;
    }
}