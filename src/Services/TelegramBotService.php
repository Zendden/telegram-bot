<?php 

namespace App\Services;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class TelegramBotService 
{
    /**
     * Telegram-bot api methods.
     */
    public const GET_UPDATES_METHOD     = 'getUpdates';
    public const SEND_MESSAGE_METHOD    = 'sendMessage';

    /**
     * @var DataValidatorService
     */
    private DataValidatorService $dataValidatorService;

    /**
     * @var QueueMessageService
     */
    private QueueMessageService $queueMessageService;

    /**
     * @var string
     */
    private string $token;

    /**
     * @var string
     */
    private string $baseTelegramApiUrl;

    /**
     * @param ParameterBagInterface $parameterBag
     * @param DataValidatorService $dataValidatorService
     * @param QueueMessageService $queueMessageService
     */
    public function __construct(
        ParameterBagInterface $parameterBag, 
        DataValidatorService $dataValidatorService,
        QueueMessageService $queueMessageService
    ){
        $this->token                = $parameterBag->get('telegram-api-token');
        $this->baseTelegramApiUrl   = $parameterBag->get('telegram-api-url');

        $this->dataValidatorService = $dataValidatorService;
        $this->queueMessageService  = $queueMessageService;
    }

    /**
     * Checks required dependencies of class methods.
     *
     * @return bool
     *
     * @throws \LogicException
     */
    private function checkRequirements(): bool 
    {
        if (
            empty($this->token) || 
            empty($this->baseTelegramApiUrl)
        ) {
            throw new \LogicException(
                sprintf(
                    'Unable to get updates: [%s]',
                    empty($this->token) || empty($this->baseTelegramApiUrl)
                        ? 'token and base telegram bot url passed on null'
                        : (empty($this->token) 
                            ? 'token passed on null'
                            : 'base telegram url passed on null')
                ),
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return true;
    }

    /**
     * Makes a request to api-telegram to receive unprocessed messages.
     * 
     * @return array
     * 
     * @throws \RuntimeException
     * @throws \LogicException
     */
    public function getUpdates(): array 
    {
        $offset = $this->queueMessageService->getQueueOffset();

        if (!is_null($offset)) {
            $queryParams = [
                'offset' => $offset
            ];
        }

        try {
            if ($this->checkRequirements()) {
                $response = (HttpClient::create())->request(
                    'GET',
                    sprintf(
                        '%s/bot%s/%s',
                        $this->baseTelegramApiUrl,
                        $this->token,
                        self::GET_UPDATES_METHOD
                    ),
                    [
                        'query' => $queryParams ?? []
                    ]
                );
    
                return $response->toArray();
            }
        } catch (\Throwable $e) {
            throw new \RuntimeException(
                sprintf(
                    'Unable to get updates: [%s]', 
                    $e->getMessage()
                ),
                $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * Sends a message to the specified telegram channel.
     * 
     * @param array $queryData
     * 
     * @return bool
     * 
     * @throws \RuntimeException
     */
    public function sendMessage(array $queryData): bool
    {
        try {
            if ($this->checkRequirements()) {
                $response = (HttpClient::create())->request(
                    'POST',
                    sprintf(
                        '%s/bot%s/%s',
                        $this->baseTelegramApiUrl,
                        $this->token,
                        self::SEND_MESSAGE_METHOD
                    ),
                    [
                        'query' => $this->dataValidatorService
                            ->setDataStructure(['chat_id', 'text'])
                            ->getValidatedData($queryData),
                    ]
                );

                if ($response->getStatusCode() === Response::HTTP_OK) {
                    $response = $response->toArray();

                    if (isset($response['ok']) && $response['ok']) {
                        return true;
                    }
                }
            }
        } catch (\Throwable $e) {
            throw new \RuntimeException(
                sprintf('Unable to send message: [%s]', $e->getMessage()),
                $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return false;
    }
}