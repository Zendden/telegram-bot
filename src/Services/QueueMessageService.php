<?php 

namespace App\Services;

use App\Entity\User;
use App\Entity\QueueMessage;
use Doctrine\ORM\AbstractQuery;
use App\Services\DataValidatorService;
use App\Repository\QueueMessageRepository;

class QueueMessageService 
{
    /**
     * @var DataValidatorService
     */
    private $dataValidatorService;

    /**
     * @var QueueMessageRepository
     */
    private $queueMessageRepository;

    /**
     * @param DataValidatorService $dataValidatorService
     * @param QueueMessageRepository $queueMessageRepository
     */
    public function __construct(
        DataValidatorService $dataValidatorService,
        QueueMessageRepository $queueMessageRepository
    ){
        $this->dataValidatorService = $dataValidatorService;
        $this->queueMessageRepository = $queueMessageRepository;
    }

    /**
     * @return int|null
     */
    public function getQueueOffset(): ?int 
    {
        $offset = current($this->queueMessageRepository->createQueryBuilder('q')
            ->select('q.queueId')
            ->orderBy('q.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_SCALAR));
        
        if (isset($offset['queueId']) && $offset['queueId']) {
            return $offset['queueId'] + 1;
        }

        return null;
    }

    /**
     * @param array $updateEvent
     * @param User $user
     * 
     * @return QueueMessage
     */
    public function addQueueMessage(array $updateEvent, User $user): QueueMessage 
    {
        return $this->queueMessageRepository->save(
            (new QueueMessage())
                ->setMessage(
                    $this->dataValidatorService->get(
                        $updateEvent, 
                        ['message', 'text'], 
                        DataValidatorService::STRICT_MODE
                    )
                )
                ->setQueueId(
                    $this->dataValidatorService->get(
                        $updateEvent,
                        ['update_id'],
                        DataValidatorService::STRICT_MODE
                    )
                )
                ->setChat($user->getChat())
                ->setCreatedAt(new \DateTime())
        );
    }
}