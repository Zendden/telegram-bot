<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

class DataValidatorService
{
    /** 
     * Value for method `get`, includes strict regime.
     */
    public const STRICT_MODE = true;

    /**
     * @var bool
     */
    private $isProdEnviroment;

    /**
     * @var array
     */
    private $dataStructure = [];

    /**
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel) {
        $this->isProdEnviroment = $kernel->getEnvironment() === 'prod';
    }

    /**
     * @param Request $request
     * 
     * Return a valid-data of Request.
     * @return array
     * 
     * @throws ValidatorException
     */
    public function validateRequest(Request $request): array
    {
        return $this->getValidatedData(
            array_merge(
                $request->query->all(),
                $request->request->all(),
                $this->prepareData(json_decode($request->getContent()))
            )
        );
    }

    /**
     * @param Request $request
     * 
     * Return a Request data witout validation.
     * @return array
     */
    public function getRequestData(Request $request): array 
    {
        return $this->prepareData(
            array_merge(
                $request->query->all(),
                $request->request->all(),
                $this->prepareData(json_decode($request->getContent()))
            )
        );
    }

    /**
     * @param ResponseInterface $response
     * 
     * @return array
     */
    public function validateResponse(ResponseInterface $response): array
    {
        return $this->getValidatedData(
            json_decode($response->getContent())
        );
    }

    /**
     * @param array $structure
     * 
     * @return self
     */
    public function setDataStructure(array $structure): self
    {
        $this->dataStructure = $structure;

        return $this;
    }

    /**
     * @param array $data
     * 
     * @return array
     * 
     * @throws ValidatorException
     * @throws \LogicException
     */
    public function getValidatedData($data = []): array
    {
        if (empty($this->dataStructure)) {
            throw new ValidatorException(
                'Структура данных не установлена.', 
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        $validatedData = $this->prepareData($data);

        foreach ($this->dataStructure as $key => $value) {
            if (is_array($value)) {
                $nestedTitles = $this->dataStructure[$key];

                array_unshift($nestedTitles, $key);

                $this->get($validatedData, $nestedTitles, self::STRICT_MODE);
            } else {
                $this->get($validatedData, [$value], self::STRICT_MODE);
            }
        }

        return $validatedData;
    }

    /**
     * @param mixed $rawData
     * 
     * @return array
     */
    private function prepareData($rawData): array
    {
        if ($rawData) {
            if (is_iterable($rawData) || is_object($rawData)) {
                foreach ($rawData as $title => $item) {
                    $preapredData[$title] = $item;
                }
            } else {
                throw new ValidatorException(
                    sprintf(
                        'Переданые данные [тип данных: `%s`] должны должны быть совместимы с интерфейсом `Iterator`.',
                        gettype($rawData)
                    )
                );
            }
        }

        return $preapredData ?? [];
    }

    /**
     * $data - target data array or object.
     * @param array|object $data
     *
     * $title - array of properties|keys titles.
     * @param array $titles
     *
     * $strict - if $strict is true, then propery or key must be found, if $strict is false, will returned null value.
     * @param bool $strict
     *
     * @return mixed
     *
     * @throws ValidatorException
     */
    public function get($data, array $titles, bool $strict = false)
    {
        if (empty($titles)) {
            throw new ValidatorException(
                'Набор названий свойств|ключей не указан.',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        foreach ($titles as $title) {
            try {
                $value = $this->getValue($value ?? $data, $title, $strict);
            } catch (ValidatorException $e) {
                throw new ValidatorException(
                    sprintf('%s Цепь значений `%s`.', $e->getMessage(), implode('->', $titles)),
                    Response::HTTP_BAD_REQUEST
                );
            }
        }

        return $value;
    }

    /**
     * $data - target data array or object.
     * @param array|object $data
     *
     * $title - array of properties|keys titles.
     * @param array $titles
     *
     * $strict - if $strict is true, then propery or key must be found, if $strict is false, will returned null value.
     * @param bool $strict
     *
     * @return mixed
     *
     * @throws ValidatorException
     */
    private function getValue($data, string $title, bool $strict)
    {
        $value = null;

        if (is_array($data) && array_key_exists($title, $data)) {
            $value = $data[$title];
        } elseif (is_object($data) && property_exists($data, $title)) {
            $value = $data->$title;
        } elseif ($strict) {
            throw new ValidatorException(
                sprintf('Не передан обязательный параметер `%s`.', $title),
                Response::HTTP_BAD_REQUEST
            );
        }

        return $value;
    }

    /**
     * @return bool
     */
    public function isProdEnviroment(): bool
    {
        return $this->isProdEnviroment;
    }

    /**
     * @param string $email
     *
     * @return string
     *
     * @throws ValidatorException
     */
    public function validateEmail(string $email): string
    {
        if (empty($email) || filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $email;
        }

        throw new ValidatorException(
            sprintf('Email имеет некорректное значение `%s`.', $email),
            Response::HTTP_BAD_REQUEST
        );
    }
}
