<?php

namespace App\Services;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Services\ChatService;

class UserService
{
    /**
     * @var DataValidatorService
     */
    private $dataValidatorService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ChatService
     */
    private $chatService;

    /**
     * @var ActionsService
     */
    private $actionsService;

    /**
     * @param DataValidatorService $dataValidatorService
     * @param UserRepository $userRepository
     * @param ChatService $chatService
     * @param ActionsService $actionsService
     */
    public function __construct(
        DataValidatorService $dataValidatorService,
        UserRepository $userRepository,
        ChatService $chatService,
        ActionsService $actionsService
    ) {
        $this->dataValidatorService = $dataValidatorService;
        $this->userRepository = $userRepository;
        $this->chatService = $chatService;
        $this->actionsService = $actionsService;
    }

    /**
     * @param array $updateEvent
     *
     * @return User
     */
    public function createUser(array $updateEvent): User
    {
        /** @var User */
        $newUser = $this->buildUser(
            $this->dataValidatorService->get(
                $updateEvent,
                ['message', 'from'],
                DataValidatorService::STRICT_MODE
            ),
            $this->dataValidatorService->get(
                $updateEvent,
                ['message', 'chat'],
                DataValidatorService::STRICT_MODE
            )
        );

        return $this->userRepository->save($newUser);
    }

    /**
     * @param array $updateEvent
     * 
     * @return User|bool
     */
    public function getUser(array $updateEvent) 
    {
        /** @var User[] */
        $foundUser = $this->userRepository->findBy([
            'username' => $this->dataValidatorService->get(
                $updateEvent,
                ['message', 'from', 'username'],
                DataValidatorService::STRICT_MODE
            ),
        ]);

        return current($foundUser);
    }

    /**
     * @param array $fromData
     * @param array $chatData
     *
     * @return User
     */
    private function buildUser(array $fromData, array $chatData): User
    {
        return (new User())
            ->setFirstname(
                $this->dataValidatorService->get(
                    $fromData,
                    ['first_name']
                )
            )
            ->setLastname(
                $this->dataValidatorService->get(
                    $fromData,
                    ['last_name']
                )
            )
            ->setUsername(
                $this->dataValidatorService->get(
                    $fromData,
                    ['username'],
                    DataValidatorService::STRICT_MODE
                )
            )
            ->setChat(
                $this->chatService->buildChat(
                    $chatData
                )
            )
            ->setCreatedAt(new \DateTime());
    }
}
