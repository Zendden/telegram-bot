<?php

namespace App\Command;

use App\Controller\TelegramBotController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class TelegramBotCommand extends Command
{
    protected static $defaultName = 'demon:telegram-bot:start';

    /**
     * @var TelegramBotController
     */
    private $telegramBotController;

    /**
     * @param string $name
     * @param TelegramBotController $telegramBotController
     */
    public function __construct(
        string $name = null,
        TelegramBotController $telegramBotController
    ) {
        parent::__construct($name);

        $this->telegramBotController = $telegramBotController;
    }

    protected function configure()
    {
        $this
            ->setDescription('Demon for telegram-bot.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        $io->success('Demon started.');

        while(true) {
            try {
                $io->text($this->telegramBotController->processUpdates());

                $io->text(
                    sprintf(
                        'Memory usage: %s', 
                        round(memory_get_usage() / 1024 / 1024 / 1024, 4)
                    )
                );
            } catch (\Throwable $e) {
                $io->note($e->getMessage());
            }

            sleep(5);
        }
    }
}
