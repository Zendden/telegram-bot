<?php

namespace App\Controller;

use OpenApi\Annotations as SWG;
use App\Services\TelegramBotService;
use App\Services\UpdateEventService;
use App\Services\DataValidatorService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * @Route("/api/v1/telegram")
 */
class TelegramBotController extends AbstractFOSRestController
{
    /**
     * Statuses of response.
     */
    public const SUCCESS    = 'success';
    public const ERROR      = 'error';

    /**
     * @var TelegramBotService
     */
    private TelegramBotService $telegramBotService;

    /**
     * @var DataValidatorService
     */
    private DataValidatorService $dataValidatorService;

    /**
     * @var UpdateEventService
     */
    private UpdateEventService $updateEventService;

    /**
     * @param TelegramBotService $telegramBotService
     * @param DataValidatorService $dataValidatorService
     * @param UpdateEventService $updateEventService
     */
    public function __construct(
        TelegramBotService $telegramBotService,
        DataValidatorService $dataValidatorService,
        UpdateEventService $updateEventService
    ) {
        $this->telegramBotService   = $telegramBotService;
        $this->dataValidatorService = $dataValidatorService;
        $this->updateEventService   = $updateEventService;
    }

    /**
     * Get unprocessed messages from telegram.
     *
     * @Rest\Get("/")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Успешно",
     *     @SWG\Schema(
     *          @SWG\Property(property="data", type="object",
     *              @SWG\Schema(
     *                  @SWG\Property(property="status", type="string")
     *              )
     *          ),
     *          @SWG\Property(property="code", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response=422,
     *     description="Получен невалидный ответ от api-telegram",
     *     @SWG\Schema(
     *          @SWG\Property(property="data", type="object",
     *              @SWG\Schema(
     *                  @SWG\Property(property="message", type="string")
     *              )
     *          ),
     *          @SWG\Property(property="code", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Внутренняя ошибка сервера, обратитесь к администратору",
     *     @SWG\Schema(
     *          @SWG\Property(property="data", type="object",
     *              @SWG\Schema(
     *                  @SWG\Property(property="message", type="string")
     *              )
     *          ),
     *          @SWG\Property(property="code", type="string")
     *     )
     * )
     *
     * @return JsonResponse
     */
    public function processUpdates(): JsonResponse
    {
        try {
            $updateEvents = $this->dataValidatorService->get(
                $this->telegramBotService->getUpdates(),
                ['result'],
                DataValidatorService::STRICT_MODE
            );

            if (!empty($updateEvents)) {
                $isUpdated = $this->updateEventService->processUpdates($updateEvents);
            }

            $responseCode = Response::HTTP_OK;
            $responseData = [
                'code' => $responseCode,
                'data' => [
                    'status' => $isUpdated ? self::SUCCESS : self::ERROR
                ]
            ];
        } catch (ValidatorException $eValidator) {
            $responseCode = $eValidator->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR;
            $responseData = [
                'data' => [
                    'message' => $eValidator->getMessage()
                ],
                'code' => $responseCode
            ];
        }

        return new JsonResponse($responseData, $responseCode);
    }

    /**
     * Send message to telegram.
     *
     * @Rest\Post("/")
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Параметры: id чата, текст сообщения",
     *     required=true,
     *     @SWG\Schema(
     *          @SWG\Property(property="chat_id", type="string"),
     *          @SWG\Property(property="text", type="string")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Успешно",
     *     @SWG\Schema(
     *          @SWG\Property(property="data", type="object",
     *              @SWG\Schema(
     *                  @SWG\Property(property="status", type="string")
     *              )
     *          ),
     *          @SWG\Property(property="code", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Переданы неверные параметры",
     *     @SWG\Schema(
     *          @SWG\Property(property="data", type="object",
     *              @SWG\Schema(
     *                  @SWG\Property(property="message", type="string")
     *              )
     *          ),
     *          @SWG\Property(property="code", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Внутренняя ошибка сервера, обратитесь к администратору",
     *     @SWG\Schema(
     *          @SWG\Property(property="data", type="object",
     *              @SWG\Schema(
     *                  @SWG\Property(property="message", type="string")
     *              )
     *          ),
     *          @SWG\Property(property="code", type="string")
     *     )
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function sendMessage(Request $request): JsonResponse
    {
        try {
            $requestData = $this->dataValidatorService
                ->setDataStructure(['chat_id, text'])
                ->validateRequest($request);

            $isSent = $this->telegramBotService->sendMessage([
                'chat_id'   => $requestData['chat_id'],
                'text'      => $requestData['text']
            ]);

            $responseCode = Response::HTTP_OK;
            $responseData = [
                'data' => [
                    'status' => $isSent ? self::SUCCESS : self::ERROR
                ],
                'code' => $responseCode
            ];
        } catch (ValidatorException $eValidator) {
            $responseData = [
                'data' => [
                    'message' => $eValidator->getMessage(),
                ],
                'code' => $eValidator->getCode() ?: Response::HTTP_BAD_REQUEST
            ];
        } catch (\Throwable $eThrowable) {
            $responseCode = $eThrowable->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR;
            $responseData = [
                'data' => [
                    'message' => $eThrowable->getMessage(),
                ],
                'code' => $responseCode
            ];
        }

        return new JsonResponse($responseData, $responseCode);
    }
}
