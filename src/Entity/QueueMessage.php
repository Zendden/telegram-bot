<?php

namespace App\Entity;

use App\Entity\Chat;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QueueMessageRepository")
 */
class QueueMessage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=128)
     */
    private $queueId;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $message;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Chat", cascade={"persist", "remove"})
     */
    private $chat;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\Version()
     */
    private $createdAt;

    /**
     * @return int|null
     */
    public function getId(): ?int 
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getQueueId(): int 
    {
        return $this->queueId;
    }

    /**
     * @param int $queueId
     * 
     * @return self
     */
    public function setQueueId(int $queueId): self 
    {
        $this->queueId = $queueId;

        return $this;
    }
    
    /**
     * @return string
     */
    public function getMessage(): string 
    {
        return $this->message;
    }

    /**
     * @param string $message
     * 
     * @return self
     */
    public function setMessage(string $message): self 
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return Chat
     */
    public function getChat(): Chat 
    {
        return $this->chat;
    }

    /**
     * @param Chat $chat
     * 
     * @return self
     */
    public function setChat(Chat $chat): self 
    {
        $this->chat = $chat;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime 
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * 
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt): self 
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
