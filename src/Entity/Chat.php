<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChatRepository")
 */
class Chat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=128)
     */
    private $chatId;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\Version()
     */
    private $createdAt;

    /**
     * @return int|null
     */
    public function getId(): ?int 
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getChatId(): int 
    {
        return $this->chatId;
    }

    /**
     * @param int $chatId
     * 
     * @return self
     */
    public function setChatId(int $chatId): self 
    {
        $this->chatId = $chatId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime 
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * 
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt): self 
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
