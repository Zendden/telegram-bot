<?php

namespace App\Entity;

use App\Entity\Chat;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $username;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Chat", cascade={"persist", "remove"})
     */
    private $chat;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\Version()
     */
    private $createdAt;

    /**
     * @return int|null
     */
    public function getId(): ?int 
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFirstname(): ?string 
    {
        return $this->firstname;
    }

    /**
     * @param string|null $firstname
     * 
     * @return self
     */
    public function setFirstname(?string $firstname): self 
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string 
    {
        return $this->lastname;
    }

    /**
     * @param string|null $lastname
     * 
     * @return self
     */
    public function setLastname(?string $lastname): self 
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string 
    {
        return $this->username;
    }

    /**
     * @param string $username
     * 
     * @return self
     */
    public function setUsername(string $username): self 
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return Chat
     */
    public function getChat(): Chat 
    {
        return $this->chat;
    }

    /**
     * @param Chat $chat
     * 
     * @return self
     */
    public function setChat(Chat $chat): self 
    {
        $this->chat = $chat;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime 
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * 
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt): self 
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
