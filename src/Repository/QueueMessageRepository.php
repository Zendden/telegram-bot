<?php

namespace App\Repository;

use App\Entity\QueueMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method QueueMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method QueueMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method QueueMessage[]    findAll()
 * @method QueueMessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QueueMessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QueueMessage::class);
    }

    /**
     * @param QueueMessage|QueueMessage[] $queueMessage
     * 
     * @return QueueMessage|QueueMessage[]
     */
    public function save($queueMessage) 
    {
        $em = $this->getEntityManager();

        if (is_array($queueMessage)) {
            foreach ($queueMessage as $eachQueueMessage) {
                if (!$eachQueueMessage->getId()) {
                    $em->persist($eachQueueMessage);
                }
            }
        } else {
            if (!$queueMessage->getId()) {
                $em->persist($queueMessage);
            }
        }
        

        $em->flush();

        return $queueMessage;
    }
}
