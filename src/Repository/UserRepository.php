<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param User|User[] $user
     * 
     * @return User|User[]
     */
    public function save($user) 
    {
        $em = $this->getEntityManager();

        if (is_array($user)) {
            foreach ($user as $eachUser) {
                if (!$eachUser->getId()) {
                    $em->persist($user);
                }
            }
        } else {
            if (!$user->getId()) {
                $em->persist($user);
            }
        }
        
        $em->flush();

        return $user;
    }
}
